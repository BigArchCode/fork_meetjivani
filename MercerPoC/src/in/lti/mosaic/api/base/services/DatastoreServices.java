package in.lti.mosaic.api.base.services;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import in.lnt.constants.Constants;
import in.lnt.utility.constants.LoggerConstants;
import in.lti.mosaic.api.base.configs.Cache;
import in.lti.mosaic.api.base.configs.CacheConstants;
import in.lti.mosaic.api.base.exceptions.ExceptionsMessanger;
import in.lti.mosaic.api.base.exceptions.SystemException;
import in.lti.mosaic.api.base.loggers.ParamUtils;
import in.lti.mosaic.api.base.serializer.ObjectSerializationHandler;

/**
 * 
 * @author Balkrushna Patil
 * 
 *         Serivces to handle datastore mechanism
 */
public class DatastoreServices {

	private static final Logger logger = LoggerFactory.getLogger(DatastoreServices.class);
  
	private DatastoreServices() {
		super();
	}
	
  /**
   * 
   * @param data
   * @param fileName
   * @param url
   * @return documentId
   * @throws IOException
   * @throws SystemException
   */
	public static String writeDocs(String data, String fileName) throws IOException, SystemException {
		logger.debug("{}  {}  {}", LoggerConstants.LOG_MAXIQAPI, Constants.LOG_WRITEDOCS,
				ParamUtils.getString(data, fileName));
		long docStoreWriteTimeout = 300;
		TimeUnit timeUnit = TimeUnit.SECONDS;
		try {
			String cacheDocStoreWriteTimeout = Cache.getProperty(CacheConstants.DOCSTORE_WRITE_TIMEOUT);
			if (null != cacheDocStoreWriteTimeout) {
				docStoreWriteTimeout = Long.parseLong(cacheDocStoreWriteTimeout);
			}

			String cacheTimeUnit = Cache.getProperty(CacheConstants.DOCSTORE_WRITE_TIMEOUT_UNIT);
			if (null != cacheTimeUnit) {
				timeUnit = getTimeOutUnit(cacheTimeUnit);
			}
			logger.debug("{} {} DOCSTORE_WRITE_TIMEOUT : {} {}", LoggerConstants.LOG_MAXIQAPI, Constants.LOG_WRITEDOCS,
					docStoreWriteTimeout, timeUnit);
		} catch (NumberFormatException nfe) {
			logger.debug(LoggerConstants.LOG_MAXIQAPI + Constants.LOG_WRITEDOCS
					+ "Incorrect value for DOCSTORE_WRITE_TIMEOUT in properties file.");
		}
		OkHttpClient client = new OkHttpClient();
		client.setWriteTimeout(docStoreWriteTimeout, timeUnit);
		client.setReadTimeout(docStoreWriteTimeout, timeUnit);
		String documentId = null;

		MediaType mediaType = MediaType.parse("multipart/form-data;");
		String bucketId = Cache.getProperty(CacheConstants.BUCKET_ID);
		String apiKey = Cache.getProperty(CacheConstants.API_KEY);
		String url = Cache.getProperty(CacheConstants.DOCUMENT_STORE_URL);
		RequestBody body = RequestBody.create(mediaType, data);

		Request request = new Request.Builder().url(url + "?bucketId=" + bucketId + "&fileName=" + fileName).post(body)
				.addHeader("apikey", apiKey).addHeader("cache-control", "no-cache").build();

		Response response = client.newCall(request).execute();

		String docsResponse = response.body().string();

		/* converting string json to map */
		Map convertJsonToMap = ObjectSerializationHandler.convertLoggerStringToMap(docsResponse);
		Object dataObject = convertJsonToMap.get("data");
		if (null != dataObject) {
			/* converting object to map */
			ObjectMapper mapper = new ObjectMapper();
			Map convertValue = mapper.convertValue(dataObject, Map.class);
			if (null != convertValue) {
				documentId = (String) convertValue.get("_id");
			}
		}
		if (null == documentId) {
			ExceptionsMessanger.throwException(new SystemException(), "ERR_005", response.message());
		}
		logger.debug("{}  : << writeDocs() << {}", LoggerConstants.LOG_MAXIQAPI, ParamUtils.getString(documentId));
		return documentId;

	}

  /**
   * 
   * @param documentId
   * @return
   * @throws IOException
   * @throws SystemException 
   */

	public static String download(String documentId) throws IOException, SystemException {
		logger.debug("{}  : >> download() << {}", LoggerConstants.LOG_MAXIQAPI, ParamUtils.getString(documentId));
		long docStoreReadTimeout = 300;
		TimeUnit timeUnit = TimeUnit.SECONDS;
		try {
			String cacheDocStoreReadTimeout = Cache.getProperty(CacheConstants.DOCSTORE_READ_TIMEOUT);
			if (null != cacheDocStoreReadTimeout) {
				docStoreReadTimeout = Long.parseLong(cacheDocStoreReadTimeout);
			}

			String cacheTimeUnit = Cache.getProperty(CacheConstants.DOCSTORE_READ_TIMEOUT_UNIT);
			if (null != cacheTimeUnit) {
				timeUnit = getTimeOutUnit(cacheTimeUnit);
			}
			logger.debug("{} > {} > DOCSTORE_READ_TIMEOUT: {} {}", LoggerConstants.LOG_MAXIQAPI, Constants.LOG_DOWNLOAD,
					docStoreReadTimeout, timeUnit);

		} catch (NumberFormatException nfe) {
			logger.debug("{} {} Incorrect value for DOCSTORE_READ_TIMEOUT in properties file.",
					LoggerConstants.LOG_MAXIQAPI, Constants.LOG_DOWNLOAD);
		} catch (Exception e) {
			logger.debug("{} {} {}", LoggerConstants.LOG_MAXIQAPI, Constants.LOG_DOWNLOAD, e.getMessage());
		}

		OkHttpClient client = new OkHttpClient();
		client.setWriteTimeout(docStoreReadTimeout, timeUnit);
		client.setReadTimeout(docStoreReadTimeout, timeUnit);

		String downloadResponse = null;
		String apiKey = Cache.getProperty(CacheConstants.API_KEY);
		String url = Cache.getProperty(CacheConstants.DOCUMENT_STORE_URL);
		MediaType mediaType = MediaType.parse("multipart/form-data;");
		RequestBody.create(mediaType, "");
		Request request = new Request.Builder().url(url + "/" + documentId + "/download").get()
				.addHeader("apikey", apiKey).addHeader("cache-control", "no-cache").build();

		Response response = client.newCall(request).execute();
		if (StringUtils.equalsIgnoreCase("OK", response.message())) {
			downloadResponse = response.body().string();
		} else {
			logger.error("{}  response from document store is not OK", LoggerConstants.LOG_MAXIQAPI);
			ExceptionsMessanger.throwException(new SystemException(), "ERR_006", null);
		}

		logger.debug("{}  : << download() << {}", LoggerConstants.LOG_MAXIQAPI, ParamUtils.getString(downloadResponse));
		return downloadResponse;
	}

  private static TimeUnit getTimeOutUnit(String unit) {
		TimeUnit tUnit = null;
		switch(unit) {
		case "MINUTES" :
			tUnit = TimeUnit.valueOf(TimeUnit.MINUTES.name()); break;
		case "SECONDS" :
			tUnit = TimeUnit.valueOf(TimeUnit.SECONDS.name()); break;
		case "MILLISECONDS" :
			tUnit = TimeUnit.valueOf(TimeUnit.MILLISECONDS.name()); break;
		case "MICROSECONDS" :
			tUnit = TimeUnit.valueOf(TimeUnit.MICROSECONDS.name()); break;
		default :
			tUnit = TimeUnit.valueOf(TimeUnit.SECONDS.name()); break;
		}
		return tUnit;
  }
}