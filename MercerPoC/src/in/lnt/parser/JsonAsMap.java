package in.lnt.parser;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonAsMap {
	private static final Logger logger = LoggerFactory.getLogger(JsonAsMap.class);

	private JsonAsMap() {
		
	}
	
	public static List<HashMap<String, String>> driver() {
		
		String fileLocation = "C:/Users/10639498/Downloads/incumbents.json";
		File file = new File(fileLocation);

		try (InputStream is = new FileInputStream(file)) {
			return getJsonAsMap(is);
		} catch (Exception e) {
			logger.error("Driver{}",e.getMessage());
		}
		return Collections.emptyList();
	}


	public static String toString(InputStream inputStream) {

		ByteArrayOutputStream result = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int length;
		try {
			while ((length = inputStream.read(buffer)) != -1) {
				result.write(buffer, 0, length);
			}
			return result.toString(StandardCharsets.UTF_8.toString());
		} catch (IOException e) {
			
			logger.error("To string{}",e.getMessage());
		}
		return null;
	}
	
	@SuppressWarnings("deprecation")
	public static List<HashMap<String, String>> getJsonAsMap(InputStream inputStream) {
		List<HashMap<String, String>> result = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
			mapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);
			mapper.configure(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY, true);
			mapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);

			TypeReference<List<HashMap<String, String>>> typeRef = new TypeReference<List<HashMap<String, String>>>() {
			};
			result = mapper.readValue(toString(inputStream), typeRef);
			return result;
		} catch (Exception e) {
			
				return Collections.emptyList();
			
		}
	}
}
