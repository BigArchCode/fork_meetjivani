package in.lnt.parser;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;

import in.lnt.constants.Constants;
import in.lnt.exceptions.CustomStatusException;
import in.lnt.utility.constants.ErrorCacheConstant;
import in.lnt.utility.general.Cache;

public class CSVParser {
	private CSVParser() {

	}

	private static final Logger logger = LoggerFactory.getLogger(CSVParser.class);

	public static List<String[]> dataForAggregation = null;
	public static final int MAX_COLMNS = 7000;

	public static Map<String, Object> parseFile(InputStream inputStreamCSV) {
		List<String> header = new ArrayList<>();
		HashMap<String, Object> map = new HashMap<>();
		List<ArrayList<String>> rows = new ArrayList<>();
		List<HashMap<String, String>> allRecords = new ArrayList<>();
		HashSet<String> eeidSet = new HashSet<>();
		boolean eeidFlag = true;

		CsvParserSettings settings = new CsvParserSettings();
		settings.getFormat().setLineSeparator("\n");

		// Set max no of columns from 512 to MAX_COLUMNS
		settings.setMaxColumns(MAX_COLMNS);

		// creates a CSV parser
		CsvParser parser = new CsvParser(settings);

		// parses all rows in one go.
		try {
			List<String[]> allRows = parser.parseAll(inputStreamCSV);
			dataForAggregation = allRows;
			int count = 0;
			for (String[] row : allRows) {
				if (count == 0)
					header = Arrays.asList(row);
				else if(count == 1){
					continue;
				}
				else {
					ArrayList<String> tempRow = new ArrayList<>(Arrays.asList(row));
					rows.add(tempRow);
				}
				count++;
			}
		} catch (Exception e) {
			logger.error("Parse File{}", e.getMessage());

		}
		map = (HashMap<String, Object>) prepareMapFromList(header, map, rows, allRecords, eeidSet, eeidFlag);
		return map;
	}

	/**
	 * @param header
	 * @param map
	 * @param rows
	 * @param allRecords
	 * @param eeidSet
	 * @param eeidFlag
	 */
	private static Map<String, Object> prepareMapFromList(List<String> header, HashMap<String, Object> map,
			List<ArrayList<String>> rows, List<HashMap<String, String>> allRecords, HashSet<String> eeidSet,
			boolean eeidFlag) {
		for (ArrayList<String> row : rows) {

			HashMap<String, String> record = new HashMap<>();
			for (int i = 0; i < header.size(); i++) {

				eeidFlag = setEEIDFlag(header, eeidSet, eeidFlag, row, i);
				if (eeidFlag)
					try {
						record.put(header.get(i), (!StringUtils.isEmpty(row.get(i)) ? row.get(i) : ""));
					} catch (IndexOutOfBoundsException e) {
						record.put(header.get(i), "");
					}
				else {
					map.put("isEeidDuplicate", "true");
					eeidSet.clear();
					return map;
				}

			}
			allRecords.add(record);
		}
		map.put("allRecords", allRecords);
		eeidSet.clear();
		return map;
	}

	/**
	 * 
	 * @param header
	 * @param eeidSet
	 * @param eeidFlag
	 * @param row
	 * @param i
	 * @return
	 */
	private static boolean setEEIDFlag(List<String> header, HashSet<String> eeidSet, boolean eeidFlag,
			ArrayList<String> row, int i) {
		if (header.get(i).equalsIgnoreCase(Constants.EMPLOYEE_EEID)) {
			try {
				if (row.get(i) != null && !row.get(i).equals("")) {
					eeidFlag = eeidSet.add(row.get(i));
				}
			} catch (IndexOutOfBoundsException e) {
				eeidFlag = true;
			}
		}
		return eeidFlag;
	}

	public static Map<String, Object> parseFile(InputStream inputStreamCSV, boolean flag) throws CustomStatusException {
		logger.error("{}{}", Constants.INTIATE, Constants.LOG_PARSECSVFILE);
		HashMap<String, Object> map = new HashMap<>();
		List<String> header = new ArrayList<>();
		List<ArrayList<String>> rows = new ArrayList<>();
		List<HashMap<String, String>> allRecords = new ArrayList<>();
		HashSet<String> eeidSet = new HashSet<>();
		ArrayList<String> headerList = null;
		CsvParserSettings settings = new CsvParserSettings();
		settings.getFormat().setLineSeparator("\n");

		// Due to ML Integration need to increase csv column from 512 to 1600.
		settings.setMaxColumns(MAX_COLMNS);
		// creates a CSV parser
		CsvParser parser = new CsvParser(settings);
		// parses all rows in one go.

		List<String[]> allRows = parser.parseAll(inputStreamCSV);
		logger.info("allRows{}", allRows);
		dataForAggregation = allRows;
		header = prepareHeader(header, allRows, rows);
		
		Map<String, Boolean> flagMap = new HashMap<>();
		
		for (ArrayList<String> row : rows) {

			HashMap<String, String> record = new HashMap<>();
			for (int i = 0; i < header.size(); i++) {

				// PE : 2449 to check duplication of EEID in CSV
				flagMap = setFlag(flagMap, header, row, eeidSet, i);
				if (flagMap.get(Constants.EEID_FLAG)) {
					try {
						// PE-6874
						record.put(header.get(i), (!StringUtils.isEmpty(row.get(i)) ? row.get(i) : null));
					} catch (IndexOutOfBoundsException e) {
						record.put(header.get(i), Constants.BLANK);
					}
				} else {
					map.put("isEeidDuplicate", "true");
					// PE-8690
					map.put(Constants.EEID_COL_FLAG, flagMap.get(Constants.EEID_COL_FLAG));
					eeidSet.clear();
					return map;
				}
			}
			// PE-8933
			headerList = (ArrayList<String>) prepareHeaderList(map, header, allRecords, headerList, flagMap.get(Constants.EEID_COL_FLAG), 
					flagMap.get(Constants.EEID_MISSING_FLAG), record);
		}
		map = (HashMap<String, Object>) finalizeMap(map, header, allRecords, eeidSet, headerList, flagMap.get(Constants.EEID_MISSING_FLAG));
		logger.info("map{}", map);
		logger.info("{}{}", Constants.EXIT, Constants.LOG_PARSECSVFILE);
		return map;
	}
	
	private static Map<String, Boolean> setFlag(Map<String, Boolean> flagMap, List<String> header, ArrayList<String> row, HashSet<String> eeidSet, int i) {
		
		if (header.get(i).equalsIgnoreCase(Constants.EMPLOYEE_EEID)) {
			flagMap.put(Constants.EEID_COL_FLAG, true);
			try {
				if (row.get(i) != null && !row.get(i).equals(Constants.BLANK)) {
					flagMap.put(Constants.EEID_FLAG, eeidSet.add(row.get(i)));
				} else {
					// PE-8933
					flagMap.put(Constants.EEID_MISSING_FLAG, true);
				}

			} catch (IndexOutOfBoundsException e) {
				flagMap.put(Constants.EEID_FLAG, true);
			}
		}
		return flagMap;
	}

	/**
	 * @param header
	 * @param allRows
	 * @param rows
	 */
	public static List<String> prepareHeader(List<String> header, List<String[]> allRows, List<ArrayList<String>> rows) throws CustomStatusException {
		String[] blankRow = null;
		String[] nullRow = null;
		int count = 0;
		for (String[] row : allRows) {
			if (count == 0) {
				header = Arrays.asList(row);
				logger.info("header{}", header);
				// PE-8575: Reject the file in case of Header is missing for one or multiple
				// columns
				if (header.contains(null)) {
					throw new CustomStatusException(Constants.HTTP_STATUS_400,
							Cache.getPropertyFromError(ErrorCacheConstant.ERR_064));
				}
				nullRow = new String[header.size()];
				blankRow = new String[header.size()];
				for (int c = 0; c < header.size(); c++) {
					blankRow[c] = "";
				}

			} else if (count == 1) {
				continue;
			} else {
				if (!Arrays.equals(blankRow, row) && !Arrays.equals(nullRow, row)) {
					ArrayList<String> tempRow = new ArrayList<>(Arrays.asList(row));
					rows.add(tempRow);
				}
			}
			count++;
		}
		return header;
	}

	/**
	 * @param map
	 * @param header
	 * @param allRecords
	 * @param eeidSet
	 * @param headerList
	 * @param eeidColFlag
	 */
	private static Map<String, Object> finalizeMap(HashMap<String, Object> map, List<String> header,
			List<HashMap<String, String>> allRecords, HashSet<String> eeidSet, ArrayList<String> headerList,
			boolean eeidColFlag) {
		if (headerList != null) {
			map.put("header", headerList);
		} else {
			map.put("header", header);
		}
		// PE-8690
		map.put("eeidColFlag", eeidColFlag);
		map.put("allRecords", allRecords);
		eeidSet.clear();
		
		return map;
	}

	/**
	 * @param map
	 * @param header
	 * @param allRecords
	 * @param headerList
	 * @param eeidColFlag
	 * @param eeidMissingFlag
	 * @param record
	 * @return
	 */
	private static List<String> prepareHeaderList(HashMap<String, Object> map, List<String> header,
			List<HashMap<String, String>> allRecords, ArrayList<String> headerList, boolean eeidColFlag,
			boolean eeidMissingFlag, HashMap<String, String> record) {
		if (eeidMissingFlag)
			map.put(Constants.MISSING_EEID, true);
		else
			map.put(Constants.MISSING_EEID, false);

		if (!eeidColFlag) {
			headerList = new ArrayList<>();
			headerList.addAll(header);
			headerList.add(Constants.EMPLOYEE_EEID);
		}
		allRecords.add(record);
		return headerList;
	}

}
