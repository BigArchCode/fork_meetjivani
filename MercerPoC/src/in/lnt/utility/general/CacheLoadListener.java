package in.lnt.utility.general;

import java.sql.SQLException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lti.mosaic.derby.DerbyMainLoader;

import in.lnt.service.db.DBUtils;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheException;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import net.sf.ehcache.config.CacheConfiguration;

public class CacheLoadListener  implements ServletContextListener {
	private static final Logger logger = LoggerFactory.getLogger(CacheLoadListener.class);
	static CacheManager cacheMgr;
	
	static Cache cache ;

    public void contextInitialized(ServletContextEvent ctxObj) {
        loadCache();
    }

	public static void loadCache() {
		cacheMgr = CacheManager.getInstance();
       
        //2. Create a cache called "cacheStore"
        cacheMgr.addCache("cacheStore");
      
        // 3. Get a cache called "cache1"
        cache = cacheMgr.getCache("cacheStore");
        
        CacheConfiguration config = cache.getCacheConfiguration();
        config.setTimeToIdleSeconds(0);
        config.setTimeToLiveSeconds(0); 
        
        //Derby Cache Load
        DerbyMainLoader.load();

        // 4. Put db  elements in cache
        try {
			cache.put(new Element("allTableList", DBUtils.getAllTables()));
		} catch (IllegalArgumentException | IllegalStateException | CacheException | SQLException e){
			logger.error("Load Cache{}",e.getMessage());
		}
	}

    public void contextDestroyed(ServletContextEvent arg0) {
        // 8. shut down the cache manager
        cacheMgr.shutdown();
    }

}
