package in.lnt.validations;

import java.util.Iterator;
import java.util.UUID;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.lnt.constants.Constants;
import in.lnt.exceptions.CustomStatusException;
import in.lnt.utility.constants.ErrorCacheConstant;
import in.lnt.utility.general.Cache;
import in.lnt.utility.general.JsonUtils;

/**
 * @author rushikesh
 *
 */
public class PerFormL3BValidations {

  private static final Logger logger = LoggerFactory.getLogger(PerFormL3BValidations.class);

  private static ObjectMapper mapper = new ObjectMapper();
  
  private PerFormL3BValidations() {
	  
  }
	
public static JsonNode performL3B(String validationData, Boolean isMDARequest) throws Exception  {
    logger.info(" Entering into crossSectionChecks method for json body with data {}",validationData);

    String entitiesJson = "";
    String entitiesKey = null;
    Iterator<?> it = null;

    try {
    	JSONObject wholeDataObject = JsonUtils.isJSONValid(validationData); // Json Syntax checker..
      if (null != wholeDataObject) {
        it = wholeDataObject.keys();
      } else {
        throw new CustomStatusException(Constants.HTTP_STATUS_400,
            Cache.getPropertyFromError(ErrorCacheConstant.ERR_004));
      }

      //Generating UUID for a Request and Response
	  final String uuid = UUID.randomUUID().toString().replace("-", "");
      
			while (it != null && it.hasNext()) {
				entitiesKey = setEntitiesKey(entitiesKey, it);
			}

      if (entitiesKey != null && wholeDataObject.get(entitiesKey) != null) {
        // logger.info(String.format("StartTime For process : %s",
        entitiesJson = wholeDataObject.get(entitiesKey).toString();
      } else {
        throw new CustomStatusException(Constants.HTTP_STATUS_400,
            Cache.getPropertyFromError(ErrorCacheConstant.ERR_004));
      }

      //Adding Request into MongoDB
   	  FormValidator.dumpToMongo(uuid, entitiesJson, Constants.REQUEST);

      JsonNode data = validateForm(entitiesJson, isMDARequest);
      
      //Adding Response into MongoDB
   	  FormValidator.dumpToMongo(uuid, mapper.writeValueAsString(data), Constants.RESPONSE);

      return data;
    } catch (Exception e) {
      throw new Exception(e.getMessage());
    }
  }

private static String setEntitiesKey(String entitiesKey, Iterator<?> it) {
	String key;
	try {
		key = (String) it.next();
		if (key.equalsIgnoreCase("entities")) {
			entitiesKey = key;
		}
	} catch (Exception e) {
		logger.error("{}", "Key is not of String type.");
	}
	return entitiesKey;
}

    public static @ResponseBody JsonNode validateForm(String entitiesJson, Boolean isMDARequest)
      throws Exception {
    FormValidator_CSC formValidator = new FormValidator_CSC();
    return formValidator.parseAndValidateCrossSectionCheck(entitiesJson, isMDARequest);

  }

}
