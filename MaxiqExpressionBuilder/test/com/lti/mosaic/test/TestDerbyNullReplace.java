package com.lti.mosaic.test;

import org.junit.Test;
import com.lti.mosaic.function.def.LookUpFunctionBso;
import junit.framework.TestCase;

/**
 * @author rushikesh
 *
 */
public class TestDerbyNullReplace extends TestCase {
  
  @Test
  public void test() {
    String clause = "select * from abc where bcd = null and b=null anc c = 'nullable' d = 'null'"
                    + " or bcd = NULL and b=NULL anc c = 'NULLable' d = 'NULL'"
                    + " or bcd = Null and b=Null anc c = 'Nullable' d = 'Null'";
    
    
    clause = LookUpFunctionBso.handleNullCasesInClause(clause);
    System.out.println(clause);
  }

}
