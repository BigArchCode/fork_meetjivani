package com.lti.mosaic.parser.exception;


/**
 * Licensed Information -- need to work on this 
 */

/**
 *  Expception - In case function name is not in registry  
 * <p/>
 * 
 * @author Piyush
 * @since 1.0
 * @version 1.0
 */


public class InvalidFunctionName extends ExpressionEvaluatorException {
	
	private static final long serialVersionUID = 1L;

	public InvalidFunctionName(String message, Throwable throwable) {
		super(message, throwable);
	}

	public InvalidFunctionName(String message) {
		super(message);
	}
}
