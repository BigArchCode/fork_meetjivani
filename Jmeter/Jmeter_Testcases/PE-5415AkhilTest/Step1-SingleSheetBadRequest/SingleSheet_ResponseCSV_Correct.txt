{
    "entities": [
        {
            "data": [
                {
                    "city": "city21",
                    "CountryName": "US1",
                    "YOUR_EEID": "10000001",
                    "CompanyNames": "KFC1",
                    "COUNTRY_CODE": "COUNTRY_CODE21",
                    "validationErrors": [
                        {
                            "expression": "DECODE(LENGTH(this.city) > 0,true,false);",
                            "errorGroup": "Personal",
                            "errorType": "ERROR",
                            "validationType": "expression",
                            "message": "Length of Department",
                            "field": "city"
                        },
                        {
                            "expression": "DECODE(LENGTH(this.COUNTRY_CODE) > 0,true,false);",
                            "errorGroup": "Personal",
                            "errorType": "ERROR",
                            "validationType": "expression",
                            "message": "Length of Department",
                            "field": "COUNTRY_CODE"
                        }
                    ]
                },
                {
                    "city": "city31",
                    "CountryName": "US2",
                    "YOUR_EEID": "10000002",
                    "CompanyNames": "KFC2",
                    "COUNTRY_CODE": "COUNTRY_CODE31",
                    "validationErrors": [
                        {
                            "expression": "DECODE(LENGTH(this.city) > 0,true,false);",
                            "errorGroup": "Personal",
                            "errorType": "ERROR",
                            "validationType": "expression",
                            "message": "Length of Department",
                            "field": "city"
                        },
                        {
                            "expression": "DECODE(LENGTH(this.COUNTRY_CODE) > 0,true,false);",
                            "errorGroup": "Personal",
                            "errorType": "ERROR",
                            "validationType": "expression",
                            "message": "Length of Department",
                            "field": "COUNTRY_CODE"
                        }
                    ]
                },
                {
                    "city": "city41",
                    "CountryName": "US3",
                    "YOUR_EEID": "10000003",
                    "CompanyNames": "KFC3",
                    "COUNTRY_CODE": "COUNTRY_CODE41",
                    "validationErrors": [
                        {
                            "expression": "DECODE(LENGTH(this.city) > 0,true,false);",
                            "errorGroup": "Personal",
                            "errorType": "ERROR",
                            "validationType": "expression",
                            "message": "Length of Department",
                            "field": "city"
                        },
                        {
                            "expression": "DECODE(LENGTH(this.COUNTRY_CODE) > 0,true,false);",
                            "errorGroup": "Personal",
                            "errorType": "ERROR",
                            "validationType": "expression",
                            "message": "Length of Department",
                            "field": "COUNTRY_CODE"
                        }
                    ]
                },
                {
                    "city": "city51",
                    "CountryName": "US4",
                    "YOUR_EEID": "10000004",
                    "CompanyNames": "KFC4",
                    "COUNTRY_CODE": "COUNTRY_CODE51",
                    "validationErrors": [
                        {
                            "expression": "DECODE(LENGTH(this.city) > 0,true,false);",
                            "errorGroup": "Personal",
                            "errorType": "ERROR",
                            "validationType": "expression",
                            "message": "Length of Department",
                            "field": "city"
                        },
                        {
                            "expression": "DECODE(LENGTH(this.COUNTRY_CODE) > 0,true,false);",
                            "errorGroup": "Personal",
                            "errorType": "ERROR",
                            "validationType": "expression",
                            "message": "Length of Department",
                            "field": "COUNTRY_CODE"
                        }
                    ]
                },
                {
                    "city": "city61",
                    "CountryName": "US5",
                    "YOUR_EEID": "10000005",
                    "CompanyNames": "KFC5",
                    "COUNTRY_CODE": "COUNTRY_CODE61",
                    "validationErrors": [
                        {
                            "expression": "DECODE(LENGTH(this.city) > 0,true,false);",
                            "errorGroup": "Personal",
                            "errorType": "ERROR",
                            "validationType": "expression",
                            "message": "Length of Department",
                            "field": "city"
                        },
                        {
                            "expression": "DECODE(LENGTH(this.COUNTRY_CODE) > 0,true,false);",
                            "errorGroup": "Personal",
                            "errorType": "ERROR",
                            "validationType": "expression",
                            "message": "Length of Department",
                            "field": "COUNTRY_CODE"
                        }
                    ]
                },
                {
                    "city": "city71",
                    "CountryName": "US6",
                    "YOUR_EEID": "10000006",
                    "CompanyNames": "KFC6",
                    "COUNTRY_CODE": "COUNTRY_CODE71",
                    "validationErrors": [
                        {
                            "expression": "DECODE(LENGTH(this.city) > 0,true,false);",
                            "errorGroup": "Personal",
                            "errorType": "ERROR",
                            "validationType": "expression",
                            "message": "Length of Department",
                            "field": "city"
                        },
                        {
                            "expression": "DECODE(LENGTH(this.COUNTRY_CODE) > 0,true,false);",
                            "errorGroup": "Personal",
                            "errorType": "ERROR",
                            "validationType": "expression",
                            "message": "Length of Department",
                            "field": "COUNTRY_CODE"
                        }
                    ]
                },
                {
                    "city": "city81",
                    "CountryName": "US7",
                    "YOUR_EEID": "10000007",
                    "CompanyNames": "KFC7",
                    "COUNTRY_CODE": "COUNTRY_CODE81",
                    "validationErrors": [
                        {
                            "expression": "DECODE(LENGTH(this.city) > 0,true,false);",
                            "errorGroup": "Personal",
                            "errorType": "ERROR",
                            "validationType": "expression",
                            "message": "Length of Department",
                            "field": "city"
                        },
                        {
                            "expression": "DECODE(LENGTH(this.COUNTRY_CODE) > 0,true,false);",
                            "errorGroup": "Personal",
                            "errorType": "ERROR",
                            "validationType": "expression",
                            "message": "Length of Department",
                            "field": "COUNTRY_CODE"
                        }
                    ]
                },
                {
                    "city": "city91",
                    "CountryName": "US8",
                    "YOUR_EEID": "10000008",
                    "CompanyNames": "KFC8",
                    "COUNTRY_CODE": "COUNTRY_CODE91",
                    "validationErrors": [
                        {
                            "expression": "DECODE(LENGTH(this.city) > 0,true,false);",
                            "errorGroup": "Personal",
                            "errorType": "ERROR",
                            "validationType": "expression",
                            "message": "Length of Department",
                            "field": "city"
                        },
                        {
                            "expression": "DECODE(LENGTH(this.COUNTRY_CODE) > 0,true,false);",
                            "errorGroup": "Personal",
                            "errorType": "ERROR",
                            "validationType": "expression",
                            "message": "Length of Department",
                            "field": "COUNTRY_CODE"
                        }
                    ]
                }
            ],
            "contextData": {
                "uniqueIdColumnCode": "ID",
                "entityNameColumnCode": "city1",
                "campaignId": "11111",
                "grpCode": "3333",
                "companyName": "Company1",
                "cpyCode": "4444",
                "industry": {
                    "superSector": "CG",
                    "subSector": "101",
                    "sector": "100"
                },
                "sectionId": "22222",
                "orgSize": 123,
                "ctryCode": "US"
            }
        }
    ]
}