{
    "entities": [
        {
            "data": [
                {
                    "city": "city21",
                    "YOUR_EEID": "2",
                    "COUNTRY_CODE": "COUNTRY_CODE21",
                    "validationErrors": [
                        {
                            "expression": "DECODE(LENGTH(this.city) > 0,true,false);",
                            "errorGroup": "Personal",
                            "errorType": "ERROR",
                            "validationType": "expression",
                            "message": "Length of Department",
                            "field": "city"
                        },
                        {
                            "expression": "DECODE(LENGTH(this.COUNTRY_CODE) > 0,true,false);",
                            "errorGroup": "Personal",
                            "errorType": "ERROR",
                            "validationType": "expression",
                            "message": "Length of Department",
                            "field": "COUNTRY_CODE"
                        }
                    ]
                },
                {
                    "city": "city31",
                    "YOUR_EEID": "3",
                    "COUNTRY_CODE": "COUNTRY_CODE11",
                    "validationErrors": [
                        {
                            "expression": "DECODE(LENGTH(this.city) > 0,true,false);",
                            "errorGroup": "Personal",
                            "errorType": "ERROR",
                            "validationType": "expression",
                            "message": "Length of Department",
                            "field": "city"
                        },
                        {
                            "expression": "DECODE(LENGTH(this.COUNTRY_CODE) > 0,true,false);",
                            "errorGroup": "Personal",
                            "errorType": "ERROR",
                            "validationType": "expression",
                            "message": "Length of Department",
                            "field": "COUNTRY_CODE"
                        }
                    ]
                },
                {
                    "city": "city41",
                    "YOUR_EEID": "4",
                    "COUNTRY_CODE": "COUNTRY_CODE41",
                    "validationErrors": [
                        {
                            "expression": "DECODE(LENGTH(this.city) > 0,true,false);",
                            "errorGroup": "Personal",
                            "errorType": "ERROR",
                            "validationType": "expression",
                            "message": "Length of Department",
                            "field": "city"
                        },
                        {
                            "expression": "DECODE(LENGTH(this.COUNTRY_CODE) > 0,true,false);",
                            "errorGroup": "Personal",
                            "errorType": "ERROR",
                            "validationType": "expression",
                            "message": "Length of Department",
                            "field": "COUNTRY_CODE"
                        }
                    ]
                }
            ],
            "contextData": {
                "uniqueIdColumnCode": "ID",
                "entityNameColumnCode": "city1",
                "campaignId": "11111",
                "grpCode": "3333",
                "companyName": "Company1",
                "cpyCode": "4444",
                "industry": {
                    "superSector": "CG",
                    "subSector": "101",
                    "sector": "100"
                },
                "sectionId": "22222",
                "orgSize": 123,
                "ctryCode": "US"
            }
        },
        {
            "data": [
                {
                    "city": "city21",
                    "YOUR_EEID": "2",
                    "COUNTRY_CODE": "COUNTRY_CODE21",
                    "validationErrors": [
                        {
                            "expression": "DECODE(LENGTH(this.city) > 0,true,false);",
                            "errorGroup": "Personal",
                            "errorType": "ERROR",
                            "validationType": "expression",
                            "message": "Length of Department",
                            "field": "city"
                        },
                        {
                            "expression": "DECODE(LENGTH(this.COUNTRY_CODE) > 0,true,false);",
                            "errorGroup": "Personal",
                            "errorType": "ERROR",
                            "validationType": "expression",
                            "message": "Length of Department",
                            "field": "COUNTRY_CODE"
                        }
                    ]
                },
                {
                    "city": "city31",
                    "YOUR_EEID": "3",
                    "COUNTRY_CODE": "COUNTRY_CODE11",
                    "validationErrors": [
                        {
                            "expression": "DECODE(LENGTH(this.city) > 0,true,false);",
                            "errorGroup": "Personal",
                            "errorType": "ERROR",
                            "validationType": "expression",
                            "message": "Length of Department",
                            "field": "city"
                        },
                        {
                            "expression": "DECODE(LENGTH(this.COUNTRY_CODE) > 0,true,false);",
                            "errorGroup": "Personal",
                            "errorType": "ERROR",
                            "validationType": "expression",
                            "message": "Length of Department",
                            "field": "COUNTRY_CODE"
                        }
                    ]
                },
                {
                    "city": "city41",
                    "YOUR_EEID": "4",
                    "COUNTRY_CODE": "COUNTRY_CODE41",
                    "validationErrors": [
                        {
                            "expression": "DECODE(LENGTH(this.city) > 0,true,false);",
                            "errorGroup": "Personal",
                            "errorType": "ERROR",
                            "validationType": "expression",
                            "message": "Length of Department",
                            "field": "city"
                        },
                        {
                            "expression": "DECODE(LENGTH(this.COUNTRY_CODE) > 0,true,false);",
                            "errorGroup": "Personal",
                            "errorType": "ERROR",
                            "validationType": "expression",
                            "message": "Length of Department",
                            "field": "COUNTRY_CODE"
                        }
                    ]
                }
            ],
            "contextData": {
                "uniqueIdColumnCode": "ID",
                "entityNameColumnCode": "city1",
                "campaignId": "11111",
                "grpCode": "3333",
                "companyName": "Company1",
                "cpyCode": "4444",
                "industry": {
                    "superSector": "CG",
                    "subSector": "101",
                    "sector": "100"
                },
                "sectionId": "22222",
                "orgSize": 123,
                "ctryCode": "PL"
            }
        }
    ]
}





Raw



{"entities":[{"data":[{"city":"city21","YOUR_EEID":"2","COUNTRY_CODE":"COUNTRY_CODE21","validationErrors":[{"expression":"DECODE(LENGTH(this.city) > 0,true,false);","errorGroup":"Personal","errorType":"ERROR","validationType":"expression","message":"Length of Department","field":"city"},{"expression":"DECODE(LENGTH(this.COUNTRY_CODE) > 0,true,false);","errorGroup":"Personal","errorType":"ERROR","validationType":"expression","message":"Length of Department","field":"COUNTRY_CODE"}]},{"city":"city31","YOUR_EEID":"3","COUNTRY_CODE":"COUNTRY_CODE11","validationErrors":[{"expression":"DECODE(LENGTH(this.city) > 0,true,false);","errorGroup":"Personal","errorType":"ERROR","validationType":"expression","message":"Length of Department","field":"city"},{"expression":"DECODE(LENGTH(this.COUNTRY_CODE) > 0,true,false);","errorGroup":"Personal","errorType":"ERROR","validationType":"expression","message":"Length of Department","field":"COUNTRY_CODE"}]},{"city":"city41","YOUR_EEID":"4","COUNTRY_CODE":"COUNTRY_CODE41","validationErrors":[{"expression":"DECODE(LENGTH(this.city) > 0,true,false);","errorGroup":"Personal","errorType":"ERROR","validationType":"expression","message":"Length of Department","field":"city"},{"expression":"DECODE(LENGTH(this.COUNTRY_CODE) > 0,true,false);","errorGroup":"Personal","errorType":"ERROR","validationType":"expression","message":"Length of Department","field":"COUNTRY_CODE"}]}],"contextData":{"uniqueIdColumnCode":"ID","entityNameColumnCode":"city1","campaignId":"11111","grpCode":"3333","companyName":"Company1","cpyCode":"4444","industry":{"superSector":"CG","subSector":"101","sector":"100"},"sectionId":"22222","orgSize":123,"ctryCode":"US"}},{"data":[{"city":"city21","YOUR_EEID":"2","COUNTRY_CODE":"COUNTRY_CODE21","validationErrors":[{"expression":"DECODE(LENGTH(this.city) > 0,true,false);","errorGroup":"Personal","errorType":"ERROR","validationType":"expression","message":"Length of Department","field":"city"},{"expression":"DECODE(LENGTH(this.COUNTRY_CODE) > 0,true,false);","errorGroup":"Personal","errorType":"ERROR","validationType":"expression","message":"Length of Department","field":"COUNTRY_CODE"}]},{"city":"city31","YOUR_EEID":"3","COUNTRY_CODE":"COUNTRY_CODE11","validationErrors":[{"expression":"DECODE(LENGTH(this.city) > 0,true,false);","errorGroup":"Personal","errorType":"ERROR","validationType":"expression","message":"Length of Department","field":"city"},{"expression":"DECODE(LENGTH(this.COUNTRY_CODE) > 0,true,false);","errorGroup":"Personal","errorType":"ERROR","validationType":"expression","message":"Length of Department","field":"COUNTRY_CODE"}]},{"city":"city41","YOUR_EEID":"4","COUNTRY_CODE":"COUNTRY_CODE41","validationErrors":[{"expression":"DECODE(LENGTH(this.city) > 0,true,false);","errorGroup":"Personal","errorType":"ERROR","validationType":"expression","message":"Length of Department","field":"city"},{"expression":"DECODE(LENGTH(this.COUNTRY_CODE) > 0,true,false);","errorGroup":"Personal","errorType":"ERROR","validationType":"expression","message":"Length of Department","field":"COUNTRY_CODE"}]}],"contextData":{"uniqueIdColumnCode":"ID","entityNameColumnCode":"city1","campaignId":"11111","grpCode":"3333","companyName":"Company1","cpyCode":"4444","industry":{"superSector":"CG","subSector":"101","sector":"100"},"sectionId":"22222","orgSize":123,"ctryCode":"PL"}}]}