package com.lti.mosaic.api.worker.callback;

import java.io.IOException;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import in.lti.mosaic.api.base.constants.Constants;
import in.lti.mosaic.api.base.exceptions.SystemException;
import in.lti.mosaic.api.base.serializer.ObjectSerializationHandler;
import java.util.Base64;

/**
 * @author rushikesh
 *
 */
public class TestCallBack {
  
  public static void main(String[] args) {
    
	final Logger logger = LoggerFactory.getLogger(TestCallBack.class);
	  
    String callBackJson =
        createCallBackJson("ui11218_123_abc_api", "5a570f88f2be1500186b5f8f", 200);

    String callBackUrl;
    try {
      callBackUrl = args[0];

      String callBackApikey = args[1];

      sendResponseViaCallBackUrl(callBackJson, callBackUrl, RequestConstants.POST,
          callBackApikey);
    } catch (Exception e) {
    	logger.error("{}", e.getMessage());
    }
  }
  
  private static void sendResponseViaCallBackUrl(String inputJson, String urlString,
      String requestType, String apiKey) throws SystemException, IOException, InterruptedException {

    int retryNo = 1;
    HttpsURLConnection connection =
        makeHttpsConnectionCallToRequest(inputJson, urlString, requestType, null, null, apiKey);

    if (connection.getResponseCode() == 500) {
      connection.disconnect();


      while (retryNo <= 3 && connection.getResponseCode() == 500) {

        Thread.sleep(1000);

        connection =
            makeHttpsConnectionCallToRequest(inputJson, urlString, requestType, null, null, apiKey);
        retryNo++;

      }

      if (connection.getResponseCode() != 500) {
        handleResponseNot500(connection);

      } else {
        connection.disconnect();
        throw new SystemException("Call Back URL Failed(500)");
      }

    } else {
      handleResponseNot500(connection);
    }
  }
  
  private static HttpsURLConnection makeHttpsConnectionCallToRequest(String inputJson,
      String urlString, String requestType, String userName, String password, String apiKey)
      throws ConnectException   {


    URL url;
    try {

      url = new URL(urlString);
      HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

      conn.setDoOutput(true);
      conn.setRequestMethod(requestType);
      conn.setRequestProperty(RequestConstants.CONTENT_TYPE, RequestConstants.APPLICATION_JSON);
      conn.addRequestProperty(RequestConstants.ACCEPT, RequestConstants.APPLICATION_JSON);
      conn.addRequestProperty("apikey", apiKey);

      if (null != userName || null != password) {

        String authString = userName + ":" + password;

        String encoding = Base64.getEncoder().encodeToString(authString.getBytes());

        conn.setRequestProperty(RequestConstants.AUTH, RequestConstants.BASIC_AUTH + encoding);
      }


      if (inputJson != null && !inputJson.isEmpty()) {
        OutputStream os = conn.getOutputStream();
        os.write(inputJson.getBytes());
        os.flush();
      }
      return conn;
    } catch (Exception e) {
      throw new ConnectException(e.getMessage());
    }
  }
  
  private static void handleResponseNot500(HttpsURLConnection connection) throws SystemException, IOException {

    if (connection.getResponseCode() == 400) {

      connection.disconnect();

      throw new SystemException("Call Back URL Failed(400)");

    } else if (connection.getResponseCode() == 200) {
      connection.disconnect();

    } else {
      throw new SystemException("Unknown response code from call Back");
    }

  }
  
  public static String createCallBackJson(String requestId, String documentId, Integer status) {

    Map<String, Object> jsonMap = new HashMap();
    jsonMap.put(Constants.Request.REQUESTID, requestId);
    jsonMap.put(Constants.Request.DOCUMENTID, documentId);
    jsonMap.put(Constants.Request.STATUS, status);

    return ObjectSerializationHandler.toString(jsonMap);
  }
}